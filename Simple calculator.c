	#include <stdio.h>
	#include <stdlib.h>
	#include <math.h>
	#include <locale.h>
	
	// Criado por Vitor Melo
	// GitLab: https://gitlab.com/fiodustick
	
	int main() {
	
	//Variaveis================+
	float resultado, x, y;//===+
	int n1;//==================+
	
	//Se 2>1 ent�o repita o c�digo
	
	while (2 > 1) {
	
	//Lima a tela
	
	system("cls");
	
	//====//======================================================================================+
	
	//1� Passo - Descreve o que ira aparecer na tela e limpa o buffer
	printf("\n");
	printf("==============================================================================+");
	printf("\n");
	printf("\n-> Digite 1 para uma conta de soma\n");
	printf("\n-> Digite 2 para uma conta de subtracao\n");
	printf("\n-> Digite 3 para uma conta de divisao\n");
	printf("\n-> Digite 4 para uma conta de multiplicacao\n");
	printf("\n-> Digite 5 para realizar uma raiz quadrada\n");
	    printf("\n-> Digite 6 para finalizar o programa\n");
	fflush(stdout);
	printf("\n");
	scanf("%d", &n1);
	fflush(stdin);
	printf("\n");
	
	// Realiza a somat�ria
	printf("\n");
	if (n1 == 1) {
	printf("Digite o primeiro numero para somar: ");
	scanf("%f", &x);
	fflush(stdin);
	printf("\nDigite o segundo numero para somar: ");
	scanf("%f", &y);
	resultado = (x + y);
	printf("\nO resultado eh %.2f\n", resultado);
	fflush(stdout);
	}
	
	//Realiza a subtra��o
	else if (n1 == 2) {
	printf("Digite o primeiro numero para subtracao: ");
	scanf("%f", &x);
	fflush(stdin);
	printf("\nDigite o segundo numero para subtracao: ");
	scanf("%f", &y);
	resultado = (x - y);
	printf("\nO resultado eh %.2f\n", resultado);
	fflush(stdout);
	}
	
	//Realiza a divis�o
	else if (n1 == 3) {
	printf("Digite o primeiro numero para divisao: ");
	scanf("%f", &x);
	printf("\nDigite o segundo numero para divisao: ");
	scanf("%f", &y);
	fflush(stdin);
	if (x == 0 || y == 0)
	printf("\nDivisao por 0(ZERO) inexistente POR FAVOR DIGITE OUTRO NUMERO\n");
	resultado = (x / y);
	printf("\nO resultado eh %.2f\n", resultado);
	fflush(stdout);
	}
	
	//Realiza a multiplicac�o
	else if (n1 == 4) {
	printf("Digite o primeiro numero para multiplicacao: ");
	scanf("%f", &x);
	printf("\nDigite o segundo numero para multiplicacao: ");
	scanf("%f", &y);
	fflush(stdin);
	resultado = (x*y);
	printf("\nO Resultado eh:  %.2f\n", resultado);
	fflush(stdout);
	}
	//Realiza a raiz quadrada de um n�mero
	else if (n1 == 5) {
	printf("Digite o numero para ser realizado a raiz quadrada: ");
	scanf("%f", &x);
	fflush(stdin);
	resultado = (sqrt(x));
	printf("\nO Resultado eh:  %.2f\n", resultado);
	}
	
	//Caso seja digitado o numero 6 o programa sera finalizado
	else if (n1 == 6) {
	printf("PROGRAMA FINALIZADO COM SUCESSO!!!\n");
	return 0;
	}
	
	//Caso o usu�rio digitar numeros n�o declarados ira aparecer o erro "DIGITA��O INV�LIDA"
	else {
	printf("DIGITACAO INVALIDA!!!\n");
	}
	
	//Para de limpar a tela
	system("pause ");
	
	}
	return 0;
	}
